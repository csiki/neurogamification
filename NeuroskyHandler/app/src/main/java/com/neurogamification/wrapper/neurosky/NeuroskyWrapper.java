package com.neurogamification.wrapper.neurosky;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import com.neurosky.thinkgear.*;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Viktor on 2015.07.19..
 */
public class NeuroskyWrapper {

    private HandlerThread handlerThread;
    private Handler handler;
    BluetoothAdapter bluetoothAdapter;
    TGDevice tgDevice = null;
    final boolean rawEnabled = true;
    private String lastConnectionMsg = "";

    private int eegHistSize = 1000;
    private Queue<Integer> rawEEG = new ConcurrentLinkedQueue<Integer>();
    private Queue<Long> eegTime = new ConcurrentLinkedQueue<Long>();
    private volatile TGEegPower eegpow = null;
    private volatile int poorSignal = 100;
    private volatile int heartRate = 0;
    private volatile int attention = 0;
    private volatile int meditation = 0;
    private volatile int blinkStrength = 0;
    private volatile boolean lowBattery = false;

    private boolean connected = false;
    private boolean paired = false;
    private boolean found = false;

    private static NeuroskyWrapper instance = null;
    public static NeuroskyWrapper getInstance() {
        if (instance == null)
            instance = new NeuroskyWrapper();
        return instance;
    }

    /// GETTERS
    public int pollEEG() {
        if (!rawEEG.isEmpty()) return rawEEG.poll();
        return 0;
    }
    public long pollTime() {
        if (!eegTime.isEmpty()) return eegTime.poll();
        return 0;
    }
    public int getBlinkStrength() { return blinkStrength; }
    public int getPoorSignal() {
        return poorSignal;
    }
    public boolean isBatteryLow() {
        return lowBattery;
    }
    public TGEegPower getEegpow() { return eegpow; }
    public int getHeartRate() { return heartRate; }
    public int getAttention() { return attention; }
    public int getMeditation() { return meditation; }
    public int getEegNum() { return rawEEG.size(); }

    /// CONNECTION SPECIFIC METHODS
    public void tryConnect() {
        if (tgDevice.getState() != TGDevice.STATE_CONNECTING && tgDevice.getState() != TGDevice.STATE_CONNECTED)
            tgDevice.connect(rawEnabled);
    }
    public boolean isConnected() {
        return connected;
    }
    public boolean isPaired() {
        return paired;
    }
    public boolean isFound() {
        return found;
    }
    public void close() {
        tgDevice.close();
    }

    public NeuroskyWrapper() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        handlerThread = new HandlerThread("background-handler");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case TGDevice.MSG_STATE_CHANGE:

                        switch (msg.arg1) {
                            case TGDevice.STATE_IDLE:
                                found = paired = connected = false;
                                break;
                            case TGDevice.STATE_CONNECTING:
                                found = paired = true;
                                connected = false;
                                break;
                            case TGDevice.STATE_CONNECTED:
                                found = paired = connected = true;
                                tgDevice.start();
                                break;
                            case TGDevice.STATE_NOT_FOUND:
                                paired = found = connected = false;
                                break;
                            case TGDevice.STATE_NOT_PAIRED:
                                paired = connected = false;
                                found = true;
                                break;
                            case TGDevice.STATE_DISCONNECTED:
                                connected = false;
                                paired = found = true;
                        }
                        break;

                    case TGDevice.MSG_POOR_SIGNAL:
                        poorSignal = msg.arg1;
                        break;
                    case TGDevice.MSG_RAW_DATA:
                        rawEEG.add(msg.arg1);
                        eegTime.add(msg.getWhen());
                        if (rawEEG.size() > eegHistSize) {
                            rawEEG.poll();
                            eegTime.poll();
                        }
                        break;
                    case TGDevice.MSG_HEART_RATE:
                        heartRate = msg.arg1;
                        break;
                    case TGDevice.MSG_ATTENTION:
                        attention = msg.arg1;
                        break;
                    case TGDevice.MSG_MEDITATION:
                        meditation = msg.arg1;
                        break;
                    case TGDevice.MSG_BLINK:
                        blinkStrength = msg.arg1;
                        break;
                    case TGDevice.MSG_LOW_BATTERY:
                        lowBattery = true;
                        break;
                    case TGDevice.MSG_EEG_POWER:
                        eegpow = (TGEegPower) msg.obj;
                        break;
                    default:
                        break;
                }
            }
        };
        if (bluetoothAdapter != null)
            tgDevice = new TGDevice(bluetoothAdapter, handler);
    }
}
