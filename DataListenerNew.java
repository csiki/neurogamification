package com.interaxon.test.libmuse;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.interaxon.libmuse.Accelerometer;
import com.interaxon.libmuse.Eeg;
import com.interaxon.libmuse.MuseArtifactPacket;
import com.interaxon.libmuse.MuseDataListener;
import com.interaxon.libmuse.MuseDataPacket;

class DataListener extends MuseDataListener {

    private int EEGHistSize = 0;
    private int ARHistSize = 0;
    private int AccHistSize = 0;
    private Queue<Double> TP9, TP9ar;
    private Queue<Double> FP1, FP1ar;
    private Queue<Double> FP2, FP2ar;
    private Queue<Double> TP10, TP10ar;
    private Queue<Double> ForBackAcc;
    private Queue<Double> UpDownAcc;
    private Queue<Double> LRAcc;

    public DataListener(int EEGHistSize, int ARHistSize, int AccHistSize) {
        this.EEGHistSize = EEGHistSize;
        this.ARHistSize = ARHistSize;
        this.AccHistSize = AccHistSize;

        TP9 = new ConcurrentLinkedQueue<Double>();
        FP1 = new ConcurrentLinkedQueue<Double>();
        FP2 = new ConcurrentLinkedQueue<Double>();
        TP10 = new ConcurrentLinkedQueue<Double>();

        TP9ar = new ConcurrentLinkedQueue<Double>();
        FP1ar = new ConcurrentLinkedQueue<Double>();
        FP2ar = new ConcurrentLinkedQueue<Double>();
        TP10ar = new ConcurrentLinkedQueue<Double>();
    }

    public EEGpacket pollEEGpacket() {
        if (!TP9.isEmpty())
            return new EEGpacket(TP9.poll(), FP1.poll(), FP2.poll(), TP10.poll());
        return new EEGpacket();
    }

    public EEGpacket pollEEGAlphaRelativePacket() {
        if (!TP9ar.isEmpty())
            return new EEGpacket(TP9ar.poll(), FP1ar.poll(), FP2ar.poll(), TP10ar.poll());
        return new EEGpacket();
    }

    public AccPacket pollAccelerometer() {
        if (!ForBackAcc.isEmpty())
            return new AccPacket(ForBackAcc.poll(), UpDownAcc.poll(), LRAcc.poll());
        return new AccPacket();
    }

    @Override
    public void receiveMuseDataPacket(MuseDataPacket p) {
        switch (p.getPacketType()) {
            case EEG:
                updateEeg(p.getValues());
                break;
            case ACCELEROMETER:
                updateAccelerometer(p.getValues());
                break;
            case ALPHA_RELATIVE:
                updateAlphaRelative(p.getValues());
                break;
            default:
                break;
        }
    }

    @Override
    public void receiveMuseArtifactPacket(MuseArtifactPacket p) {
        if (p.getHeadbandOn() && p.getBlink()) {
            // TODO log blink
        }
    }

    private void updateAccelerometer(final ArrayList<Double> data) {
        ForBackAcc.add(data.get(Accelerometer.FORWARD_BACKWARD.ordinal()));
        UpDownAcc.add(data.get(Accelerometer.UP_DOWN.ordinal()));
        LRAcc.add(data.get(Accelerometer.LEFT_RIGHT.ordinal()));

        if (ForBackAcc.size() > AccHistSize) {
            ForBackAcc.poll();
            UpDownAcc.poll();
            LRAcc.poll();
        }
    }

    private void updateEeg(final ArrayList<Double> data) {
        TP9.add( data.get(Eeg.TP9.ordinal()) );
        FP1.add( data.get(Eeg.FP1.ordinal()) );
        FP2.add( data.get(Eeg.FP2.ordinal()) );
        TP10.add( data.get(Eeg.TP10.ordinal()) );

        if (TP9.size() > EEGHistSize) {
            TP9.poll();
            FP1.poll();
            FP2.poll();
            TP10.poll();
        }
    }

    private void updateAlphaRelative(final ArrayList<Double> data) {
        TP9ar.add(data.get(Eeg.TP9.ordinal()));
        FP1ar.add(data.get(Eeg.FP1.ordinal()));
        FP2ar.add(data.get(Eeg.FP2.ordinal()));
        TP10ar.add(data.get(Eeg.TP10.ordinal()));

        if (TP9ar.size() > ARHistSize) {
            TP9ar.poll();
            FP1ar.poll();
            FP2ar.poll();
            TP10ar.poll();
        }
    }
}
