using System.Collections.Generic;
using System.Linq;

public abstract class EEGDeviceFamily {

	private static List<EEGDeviceFamily> registeredFamilies = new List<EEGDeviceFamily>();

	static EEGDeviceFamily() {
		// TODO add families
	}

	public static List<EEGDevice> LookForAllKindsOfDevices () {
		List<EEGDevice> res = new List<EEGDevice> ();
		foreach (var family in registeredFamilies) {
			res.AddRange(family.LookForDevices());
		}
		return res;
	}

	public EEGDeviceFamily (string name) {
		
	}
	
	public abstract List<EEGDevice> LookForDevices();
}

