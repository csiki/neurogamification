﻿using UnityEngine;
using System.Collections;

// TODO support multiple devices
public class Muse : MonoBehaviour {

	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject device = null;
	private bool connected = false;

	public void Refresh() {
		//if (device == null) {
			using (deviceClass = new AndroidJavaClass("com.interaxon.test.libmuse.MuseWrapper")) {
				if (deviceClass != null) {
					Debug.Log ("Device class loaded!");
					device = deviceClass.CallStatic<AndroidJavaObject> ("getInstance");
					if (device != null) {
						Debug.Log("Device variable initiated!");
						bool isthereany = device.Call<bool>("refresh");
						if (isthereany) {
							Debug.Log("Muse device found!");
							// TODO list them
						}
					} else
						Debug.LogError("Cannot initiate device!");
				} else
					Debug.LogError ("Could not load MuseWrapper Java class!");
			}
		//}
	}

	public void Connect(int to) {
		if (device != null) {
			bool isthereany = device.Call<bool>("refresh");
			if (isthereany) {
				Debug.Log("Muse device found!");
				// TODO list them
				connected = device.Call<bool>("connect", to);
				if (connected) {
					Debug.Log("Muse device connected!");
				} else
					Debug.LogError("Cannot connect to Muse device!");
			} else
				Debug.LogError("No Muse device found!");
		} else
			Debug.LogError ("Cannot connect, no device handler is attached!");
	}

	// Use this for initialization
	void Start () {
		if (device == null) {
			using (deviceClass = new AndroidJavaClass("com.interaxon.test.libmuse.MuseWrapper")) { // TODO change package name in android studio
				if (deviceClass != null) {
					device = deviceClass.CallStatic<AndroidJavaObject>("getInstance");
					bool isthereany = device.Call<bool>("refresh");
					if (isthereany) {
						connected = device.Call<bool>("connect", 0);
						if (connected) {
							Debug.Log("Muse device connected!");
						} else
							Debug.LogError("Cannot connect to Muse device!");
					} else
						Debug.LogError("No Muse device found!");
				} else
					Debug.LogError("Could not load MuseWrapper Java class!");
			}
		}
		Time.fixedDeltaTime = 0.5f;
	}

	void FixedUpdate() {

		if (device == null) return;
		var EEGpacket = device.Call<AndroidJavaObject>("pollEEG");

		//if (EEGpacket.Get<bool> ("valid")) {
			var TP9 = EEGpacket.Get<double>("TP9");
			var FP1 = EEGpacket.Get<double>("FP1");
			var FP2 = EEGpacket.Get<double>("FP2");
			var TP10 = EEGpacket.Get<double>("TP10");
			Debug.Log("TP9: " + TP9.ToString() + "; FP1: " + FP1.ToString() + "; FP2: " + FP2.ToString() + "; TP10: " + TP10.ToString());
		//} else
		//	Debug.Log ("No EEG recording since last poll.");
	}

	void Update () {}

}
