
public enum EEGChannels {
	Nz,
	Fp1, Fp2, Fpz,
	AF3, AF4, AF7, AF8, AFz,
	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, Fz,
	FC1, FC2, FC3, FC4, FC5, FC6, FT7, FT8, FT9, FT10, FCz,
	C1, C2, C3, C4, C5, C6, T7, T8, T9, T10, A1, A2, Cz,
	CP1, CP2, CP3, CP4, CP5, CP6, TP7, TP8, TP9, TP10, CPz,
	P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, Pz,
	PO3, PO4, PO7, PO8, POz,
	O1, O2, Oz,
	Iz
}


