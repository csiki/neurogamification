﻿using UnityEngine;
using System.Collections;

public class MuseDeviceUI : MonoBehaviour {

	public GameObject museDeviceGO;
	public Muse museDevice;

	void Awake() {
		museDevice = museDeviceGO.GetComponent<Muse> ();
	}

	public void Refresh () {
		museDevice.Refresh();
	}

	public void ConnectTo(int to) {
		museDevice.Connect (to);
	}

	public void Quit() {
		Application.Quit ();
	}
}
