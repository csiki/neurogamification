using System.Collections.Generic;

public abstract class EEGDevice { // TODO külön EEGDeviceHandler ami monobehavior
	
	//////// STATIC TYPE SPECIFIC ////////
	protected static HashSet<EEGChannels> availableChannels = new HashSet<EEGChannels>();
	public static HashSet<EEGChannels> AvailableChannels {
		get {
			return availableChannels;
		}
	}
	
	public static bool IsChannelAvailable(EEGChannels chan) {
		return availableChannels.Contains(chan);
	}
	
	//////// INSTANCE SPECIFIC ////////
	public readonly string id;
	public readonly string deviceName;

	public EEGDevice(string id, string deviceName) {
		this.id = id;
		this.deviceName = deviceName;
	}

	public abstract bool Connect();
	public abstract void Disconnect();
	public abstract bool IsConnected();

	public abstract double GetEEGData(EEGChannels fromChan);
	public abstract List<double> GetEEGData(List<EEGChannels> fromChans);
	
	// TODO get eeg, and other data --> gather all kinds of data that can be retrieved from any device
}

