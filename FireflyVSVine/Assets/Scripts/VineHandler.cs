﻿using UnityEngine;
using System.Collections.Generic;

public class VineHandler : MonoBehaviour {
	
	private List<AnimVine> vines = new List<AnimVine>();
	
	// Use this for initialization
	void Start () {
		foreach (Transform child in transform)
			vines.Add(child.GetComponent<AnimVine>());
	}
	
	// Update is called once per frame
	void Update () {
		// nothing to do
	}
	
	public void Reactivate(int vineNum) {
		WithdrawAll();
		
		if (vineNum > vines.Count)
			vineNum = vines.Count;
		
		HashSet<int> randVines = new HashSet<int>();
		System.Random rnd = new System.Random();
		while (randVines.Count < vineNum) {
			int r = rnd.Next(0, vines.Count);
			if (!randVines.Contains(r))
				randVines.Add(r);
		}
		
		foreach (int vineIndex in randVines)
			vines[vineIndex].Activate();
	}
	
	public void WithdrawAll() {
		foreach (AnimVine vine in vines)
			vine.Deactivate();
	}
}
