﻿using UnityEngine;
using System.Collections;

public class StartScreenScript : MonoBehaviour {
	
	public GameObject birdGO;
	public BirdMovement bird;
	public Rigidbody2D birdBody;
	public bool sawOnce = false;
	
	public GUISkin skin;
	static float padding = 12f;
	static float generalW = 200f;
	static float generalH = 30f;
	static float labelW = 90f;
	static float labelH = 60f;
	
	public bool useDefSettings = false;
	public bool godToggle = false;
	public bool lightToggle = false;
	public float flapSpeed = 118.17f;
	public float birdSpeed = 2.04f;
	public float bounceSpeed = 125.2f;
	public float gyroMultiplier = 3.88f;
	public float gyroOffset = 0.65f;
	public float gravity = 0.51f; // TODO balfelso csap hatra mozdul !!!! 

	void Start () {
		
		if (!PlayerPrefs.HasKey("god")) {
			PlayerPrefs.SetInt("useDefSettings", useDefSettings ? 1 : 0);
			PlayerPrefs.SetInt("god", godToggle ? 1 : 0);
			PlayerPrefs.SetInt("light", lightToggle ? 1 : 0);
			PlayerPrefs.SetFloat("flapSpeed", flapSpeed);
			PlayerPrefs.SetFloat("birdSpeed", birdSpeed);
			PlayerPrefs.SetFloat("bounceSpeed", bounceSpeed);
			PlayerPrefs.SetFloat("gyroMultiplier", gyroMultiplier);
			PlayerPrefs.SetFloat("gyroOffset", gyroOffset);
			PlayerPrefs.SetFloat("gravity", gravity);
			PlayerPrefs.Save();
			Debug.Log("pref reload");
		}
		useDefSettings = PlayerPrefs.GetInt("useDefSettings") == 1;
		if (!useDefSettings) {
			godToggle = PlayerPrefs.GetInt("god") == 1;
			lightToggle = PlayerPrefs.GetInt("light") == 1;
			flapSpeed = PlayerPrefs.GetFloat("flapSpeed");
			bounceSpeed = PlayerPrefs.GetFloat("bounceSpeed");
			gyroMultiplier = PlayerPrefs.GetFloat("gyroMultiplier");
			birdSpeed = PlayerPrefs.GetFloat("birdSpeed");
			gyroOffset = PlayerPrefs.GetFloat("gyroOffset");
			gravity = PlayerPrefs.GetFloat("gravity");
		}
		
		if(!sawOnce) {
			//GetComponent<SpriteRenderer>().enabled = true;
			Time.timeScale = 0;
		}
		sawOnce = true;
		bird = birdGO.GetComponent<BirdMovement>();
		birdBody = birdGO.GetComponent<Rigidbody2D>();
	}
	
	void Update () {}
	
	void OnGUI() {
		if (sawOnce) {
			
			GUI.skin = skin;
			var startBtnRect = new Rect((Screen.width - 200) / 2f, (Screen.height - 200) / 2f - 200f, generalW, generalH);
			var usedefToggleRect = new Rect(startBtnRect.x, startBtnRect.y + startBtnRect.height + padding, generalW, generalH);
			var godToogleRect = new Rect(startBtnRect.x, usedefToggleRect.y + usedefToggleRect.height + padding, generalW, generalH);
			var lightToggleRect = new Rect(startBtnRect.x, godToogleRect.y + godToogleRect.height + padding, generalW, generalH);
			var fspeedSliderRect = new Rect(startBtnRect.x, lightToggleRect.y + lightToggleRect.height + padding, generalW, generalH);
			var fspeedLabel = new Rect(fspeedSliderRect.x - labelW, fspeedSliderRect.y - 10f, labelW, labelH);
			var bspeedSliderRect = new Rect(startBtnRect.x, fspeedSliderRect.y + fspeedSliderRect.height + padding, generalW, generalH);
			var bspeedLabel = new Rect(bspeedSliderRect.x - labelW, bspeedSliderRect.y - 10f, labelW, labelH);
			var bounceSliderRect = new Rect(startBtnRect.x, bspeedSliderRect.y + bspeedSliderRect.height + padding, generalW, generalH);
			var bounceLabel = new Rect(bounceSliderRect.x - labelW, bounceSliderRect.y - 10f, labelW, labelH);
			var gyroOffsetRect = new Rect(startBtnRect.x, bounceSliderRect.y + bounceSliderRect.height + padding, generalW, generalH);
			var gyroOffsetLabel = new Rect(gyroOffsetRect.x - labelW, gyroOffsetRect.y - 10f, labelW, labelH);
			var gyroMultiplierRect = new Rect(startBtnRect.x, gyroOffsetRect.y + gyroOffsetRect.height + padding, generalW, generalH);
			var gyroMultiplierLabel = new Rect(gyroMultiplierRect.x - labelW, gyroMultiplierRect.y - 10f, labelW, labelH);
			var gravityRect = new Rect(startBtnRect.x, gyroMultiplierRect.y + gyroMultiplierRect.height + padding, generalW, generalH);
			var gravityLabel = new Rect(gravityRect.x - labelW, gravityRect.y - 10f, labelW, labelH);
			var allSettingsLabel = new Rect(startBtnRect.x, gravityLabel.y + gravityLabel.height, generalW * 2, generalH * 5);
			
			var pushed = GUI.Button(startBtnRect, "START");
			useDefSettings = GUI.Toggle(usedefToggleRect, useDefSettings, "Forget settings");
			godToggle = GUI.Toggle(godToogleRect, godToggle, "GOD mode");
			lightToggle = GUI.Toggle(lightToggleRect, lightToggle, "Light on/off");
			flapSpeed = GUI.HorizontalSlider(fspeedSliderRect, flapSpeed, 50f, 180f);
			birdSpeed = GUI.HorizontalSlider(bspeedSliderRect, birdSpeed, 0f, 3f);
			bounceSpeed = GUI.HorizontalSlider(bounceSliderRect, bounceSpeed, 75f, 300f);
			gyroOffset = GUI.HorizontalSlider(gyroOffsetRect, gyroOffset, 0.2f, 1.2f);
			gyroMultiplier = GUI.HorizontalSlider(gyroMultiplierRect, gyroMultiplier, 1f, 7f);
			gravity = GUI.HorizontalSlider(gravityRect, gravity, 0f, 1f);
			
			GUI.Label(allSettingsLabel, "Settings:\n  flap speed: " + flapSpeed +
				"\n  bird speed: " + birdSpeed +
				"\n  bounce speed: " + bounceSpeed +
				"\n  gyro offset: " + gyroOffset +
				"\n  gyro multiplier: " + gyroMultiplier +
				"\n  gravity: " + gravity);
			GUI.Label(fspeedLabel, "Flap speed");
			GUI.Label(bspeedLabel, "Bird speed");
			GUI.Label(bounceLabel, "Bounce speed");
			GUI.Label(gyroOffsetLabel, "Gyroscope offset");
			GUI.Label(gyroMultiplierLabel, "Gyroscope multiplier");
			GUI.Label(gravityLabel, "Gravity");
			
			if (pushed) {
				
				// apply parameters
				bird.godMode = godToggle;
				bird.flapSpeed = flapSpeed;
				bird.forwardSpeed = birdSpeed;
				bird.bounceSpeed = bounceSpeed;
				bird.gyroOffset = gyroOffset;
				bird.gyroMultiplier = gyroMultiplier;
				birdBody.gravityScale = gravity;
				RenderSettings.ambientIntensity = lightToggle ? 1f : 0f;
				
				// save to playerprefs
				PlayerPrefs.SetInt("useDefSettings", useDefSettings ? 1 : 0);
				PlayerPrefs.SetInt("god", godToggle ? 1 : 0);
				PlayerPrefs.SetInt("light", lightToggle ? 1 : 0);
				PlayerPrefs.SetFloat("flapSpeed", flapSpeed);
				PlayerPrefs.SetFloat("birdSpeed", birdSpeed);
				PlayerPrefs.SetFloat("bounceSpeed", bounceSpeed);
				PlayerPrefs.SetFloat("gyroMultiplier", gyroMultiplier);
				PlayerPrefs.SetFloat("gyroOffset", gyroOffset);
				PlayerPrefs.SetFloat("gravity", gravity);
				
				PlayerPrefs.Save();
			
				Time.timeScale = 1;
				GetComponent<SpriteRenderer>().enabled = false;
				sawOnce = false;
			}
		}
	}
}
