﻿using UnityEngine;
using System.Collections;

public class AnimVine : MonoBehaviour {
	
	private Vector3 activatedPos = new Vector3();
	private Vector3 passivePos = new Vector3();
	private bool activated = false;
	private bool deactivated = false;
	
	public float journeyLen = 1f;
	public readonly float EPS = 0.001f;
	public int dir = 1; // 1 or -1
	public float startTime = -1f;
	public float deStartTime = -1f;
	public float speed = 0.01f;
	
	// Use this for initialization
	void Start () {
		passivePos = gameObject.transform.position;
		activatedPos = new Vector3(passivePos.x + journeyLen * dir, passivePos.y, passivePos.z);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (activated && startTime < Time.time) {
			float distCovered = (Time.time - startTime) * speed;
			float fracJourney = distCovered / journeyLen;
			transform.position = Vector3.Lerp(transform.position, activatedPos, fracJourney);
			if (Vector3.Distance(transform.position, activatedPos) < EPS)
				activated = false;
		}
		else if (deactivated && deStartTime < Time.time) {
			float distCovered = (Time.time - deStartTime) * speed;
			float fracJourney = distCovered / journeyLen;
			transform.position = Vector3.Lerp(transform.position, passivePos, fracJourney);
			if (Vector3.Distance(transform.position, passivePos) < EPS)
				deactivated = false;
		}
	}
	
	public void Activate() {
		startTime = Time.time + 0.4f;
		activated = true;
	}
	
	public void Deactivate() {
		deStartTime = Time.time + 0.1f;
		deactivated = true;
	}
}
