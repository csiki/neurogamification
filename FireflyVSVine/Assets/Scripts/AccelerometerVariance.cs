﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class AccelerometerVariance : MonoBehaviour {
	
	//private float lastVarCalcTime = 0;
	private float varianceTimeWindow = 0.5f;
	private LinkedList<Vector3> accHist = new LinkedList<Vector3>();
	private LinkedList<float> accMagnitudeHist = new LinkedList<float>();
	private LinkedList<float> times = new LinkedList<float>();
	//private Vector3 variance = new Vector3();
	private float magnitudeStd = 1;
	
	public float MagnitudeStd {
		get {
			return magnitudeStd;
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	void Update () {}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 acc = new Vector3(Input.acceleration.x, Input.acceleration.y, Input.acceleration.z);
		if (accHist.Count == 0 || accHist.Last.Value != acc) {
			accHist.AddLast(acc);
			accMagnitudeHist.AddLast(Input.acceleration.sqrMagnitude);
			times.AddLast(Time.time); // TODO it happens whateva
		}
		else {
			accHist.AddLast(new Vector3());
			accMagnitudeHist.AddLast(0);
			times.AddLast(Time.time);
		}
		
		if (accMagnitudeHist.Count > 0) {
			float avg = accMagnitudeHist.Average();
			float sum = 0;
			foreach (var oldacc in accMagnitudeHist) {
				sum += Mathf.Pow(oldacc - avg, 2);
			}
			magnitudeStd = Mathf.Sqrt(sum / accMagnitudeHist.Count);
			// TODO calc variance
		}
		
		while (Time.time - times.First() > varianceTimeWindow) {
			//lastVarCalcTime = Time.time;
			accHist.RemoveFirst();
			accMagnitudeHist.RemoveFirst();
			times.RemoveFirst();
		}
	}
}
