﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

// TODO decrease gravitation
public class BirdMovement : MonoBehaviour {
	
	EEGDevice device = null; // FIXME rm
	
	Vector3 velocity = Vector3.zero;
	public float flapSpeed = 10f;
	public float bounceSpeed = 10f;
	public float forwardSpeed = 1f; // TODO doesn't need to be changed according to direction
	public float rightDir = 1;
	public bool dead = false;
	public bool godMode = false;
	public GameObject leftVinesGO;
	public GameObject rightVinesGO;
	public GameObject accVarGO;
	Animator animator;
	public Color aliveParticleColor = new Color(0.9609375f, 1f, 0.32421875f);
	public Color deadParticleColor = new Color(0.89453125f, 0.2421875f, 0.2421875f);
	public ParticleSystem particle;
	public bool firstClickReceived = false;
	public StartScreenScript startScreen;
	
	public float gyroMultiplier = 3.88f;
	public float gyroOffset = 0.65f;
	
	private AccelerometerVariance accVar;
	private VineHandler leftVines;
	private VineHandler rightVines;
	private bool didFlap = false;
	private bool didHit = false;
	private float deathCooldown;
	private int round = 0;
	private LinkedList<float> auraRanges = new LinkedList<float>();
	private int auraRangeWindow = 4;
	private Light auraLight;
	
	// Use this for initialization
	void Start () {
		animator = transform.GetComponentInChildren<Animator>();
		leftVines = leftVinesGO.GetComponent<VineHandler>();
		rightVines = rightVinesGO.GetComponent<VineHandler>();
		auraLight = transform.GetComponentInChildren<Light>();
		accVar = accVarGO.GetComponent<AccelerometerVariance>();
		particle = transform.Find("particles").GetComponent<ParticleSystem>();
		particle.startColor = aliveParticleColor;
		startScreen = GameObject.Find("StartScreen").GetComponent<StartScreenScript>();

		if(animator == null) {
			Debug.LogError("Didn't find animator!");
		}
	}

	// Do Graphic & Input updates here
	void Update() {
		if (dead) {
			deathCooldown -= Time.deltaTime;

			if(deathCooldown <= 0) {
				if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) ) {
					Application.LoadLevel( Application.loadedLevel );
				}
			}
		}
		else {
			if (firstClickReceived && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
				didFlap = true;
			if (!startScreen.sawOnce && !firstClickReceived && !(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))) {
				firstClickReceived = true;
			}
		}
	}

	
	// Do physics engine updates here
	void FixedUpdate () {
		
		// FIXME rm
		if (device == null) {
			var devices = EEGDeviceFamily.LookForAllKindsOfDevices();
			if (devices.Count > 0) {
				device = devices[0];
				device.Connect();
			}
		}
		//if (device != null && !device.IsConnected())
		//	device.Connect();
		
		if (dead) return;
		
		//Debug.Log(accVar.MagnitudeStd);
		//auraLight.range = 1f / (accVar.MagnitudeStd * 2.8);
		
		// ACCELEROMETER AURA (aura range: [0, 4.5]
		if (device == null || !device.IsConnected()) {
			auraRanges.AddLast(Mathf.Max(0f, (gyroOffset - accVar.MagnitudeStd) * gyroMultiplier)); // TODO összeszűkül egy idő után a nyugiba is <-- kivonás miatt
			if (auraRanges.Count > auraRangeWindow)
				auraRanges.RemoveFirst();
			auraLight.range = auraRanges.Average();
		}
		
		// NEUROSKY eSENSE MEDIATION AURA
		if (device != null && device.IsConnected()) {
			Debug.Log(device["NeuroskyMeditationFeature"].Value);
			auraLight.range = (float)(((device["NeuroskyMeditationFeature"].Value * 4.5 / 100.0) + auraLight.range) / 2.0);
		}
		
		GetComponent<Rigidbody2D>().AddForce( Vector2.right * forwardSpeed );

		if (didFlap) {
			GetComponent<Rigidbody2D>().AddForce( Vector2.up * flapSpeed );
			animator.SetTrigger("DoFlap");
			didFlap = false;
		}
		
		if (didHit) {
			GetComponent<Rigidbody2D>().AddForce( (Vector2.right * rightDir * -1f) * bounceSpeed );
			animator.SetTrigger("DoFlap");
			didHit = false;
		}

		if (GetComponent<Rigidbody2D>().velocity.y > 0) {
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
		else {
			float angle = Mathf.Lerp (0, -30 * rightDir, (-GetComponent<Rigidbody2D>().velocity.y / 3f) );
			transform.rotation = Quaternion.Euler(0, 0, angle);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if (collision.gameObject.tag == "Deadly" && !godMode) {
			animator.SetTrigger ("Death");
			particle.startColor = deadParticleColor;
			dead = true;
			deathCooldown = 0.5f;
		}
		else if (collision.gameObject.tag == "Bouncy") {
			forwardSpeed *= -1;
			rightDir *= -1;
			Vector3 tmp = transform.Find ("BirdGFX").transform.forward;
			tmp.z *= -1;
			transform.Find ("BirdGFX").transform.forward = tmp;
			didHit = true;
			++round;
			// TODO stop particle
			if (rightDir > 0) {
				leftVines.WithdrawAll();
				rightVines.Reactivate(round / 5 + 1); // TODO finegrain
			}
			else {
				rightVines.WithdrawAll();
				leftVines.Reactivate(round / 5 + 1);
			}
		}
	}
}
