using UnityEngine;
using System.Collections.Generic;

public class MuseDevice : EEGDevice {
	
	private int museID = 0;
	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private bool connected = false;
	
	public void Init(EEGDeviceFamily family, float eegHistoryLen, int museID, AndroidJavaClass deviceClass, AndroidJavaObject deviceHandler) {
		InitDevice(family.Name + museID, family, eegHistoryLen);
		this.museID = museID;
		this.deviceClass = deviceClass;
		this.deviceHandler = deviceHandler;
		
		// TODO add muse specific features
		RegisterFeature(new MuseAlphaRelativeFeature(this, deviceHandler), false);
	}
	
	// CONNECTION
	public override bool Connect() { // TODO repair android side at connect() !!!
		if (deviceHandler != null) {
			bool isthereany = deviceHandler.Call<bool>("refresh");
			if (isthereany && deviceHandler.Call<bool>("connect", museID)) {
				connected = true;
				return true;
			}
		}
		return false;
	}
	public override void Disconnect() {
		deviceHandler.Call("disconnect", museID);
		connected = false;
	}
	public override bool IsConnected() {
		return connected;
	}
	public override float SignalQuality ()
	{
		// TODO
		return 1f;
	}
	
	// RETREIVER METHODS
	protected override void RetreiveEEG() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG"/*, museID*/); // TODO fix when android side updated
		while (eegPacket.Get<bool> ("valid")) {
			packets.Add(eegPacket);
			eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG"/*, museID*/); // TODO fix when android side updated
		}
		float dtime = Time.fixedDeltaTime / packets.Count; // TODO get this data genuine from the java side (not just calculate it here)
		float time = EEGLastUpdate + dtime;
		foreach (var packet in packets) {
			var dict = new Dictionary<EEGChannel, double>();
			dict.Add(EEGChannel.TP9, packet.Get<double>("TP9"));
			dict.Add(EEGChannel.TP10, packet.Get<double>("TP10"));
			dict.Add(EEGChannel.Fp1, packet.Get<double>("FP1"));
			dict.Add(EEGChannel.Fp2, packet.Get<double>("FP2"));
			eeg.AddLast(new KeyValuePair<float, Dictionary<EEGChannel, double>>(time, dict));
			if (time - eeg.First.Value.Key > eegHistoryLen)
				eeg.RemoveFirst();
			time += dtime;
		}
	}
	protected override void RetreiveAcceleration() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer"/*, museID*/); // TODO fix when android side updated
		while (accPacket.Get<bool> ("valid")) {
			packets.Add(accPacket);
			accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer"/*, museID*/); // TODO fix when android side updated
		}
		float dtime = Time.fixedDeltaTime / packets.Count; // TODO get this data genuine from the java side (not just calculate it here)
		float time = EEGLastUpdate + dtime;
		AccelerometerData prev = null;
		foreach (var packet in packets) {
			AccelerometerData accdata = null;
			if (prev != null)
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"), prev);
			else
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"));
			
			acc.AddLast(new KeyValuePair<float, AccelerometerData>(time, accdata));
			prev = accdata;
			if (time - acc.First.Value.Key > accHistoryLen)
				acc.RemoveFirst();
			time += dtime;
		}
	}
	
	// MONOBEHAVIOR METHODS
	protected override void Start () {}
	protected override void Update() {}
	
}

