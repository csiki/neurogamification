using UnityEngine;
using System.Collections.Generic;
using System;

public class MuseAlphaRelativeFeature : EEGFeature
{
	private AndroidJavaObject deviceHandler = null;
	
	public MuseAlphaRelativeFeature(EEGDevice device, AndroidJavaObject deviceHandler) : base(device, "MuseAlphaRelativeFeature", 0f) {
		if (device.Family.Name != "Muse")
			throw new ArgumentException("MuseAlphaRelativeFeature only works with a Muse device!");
		this.deviceHandler = deviceHandler;
	}
	
	protected override void ForceUpdate() {
		valuesSinceLastUpdate.Clear();
		var arPacket = deviceHandler.Call<AndroidJavaObject>("pollAlphaRelative");
		while (arPacket.Get<bool>("valid")) {
			var tp9 = arPacket.Get<double>("TP9");
			var tp10 = arPacket.Get<double>("TP10");
			var fp1 = arPacket.Get<double>("FP1");
			var fp2 = arPacket.Get<double>("FP2");
			double sum = 0;
			int count = 0;
			if (tp9 == tp9) { ++count; sum += tp9; }
			if (tp10 == tp10) { ++count; sum += tp10; }
			if (fp1 == fp1) { ++count; sum += fp1; }
			if (fp2 == fp2) { ++count; sum += fp2; }
			if (count > 0)
				valuesSinceLastUpdate.Add(sum / count);
			arPacket = deviceHandler.Call<AndroidJavaObject>("pollAlphaRelative");
		}
	}
}

