﻿using UnityEngine;
using System.Collections.Generic;

public class MuseDeviceUI : MonoBehaviour {

	private EEGDevice device = null;
	
	public void Refresh () {
		var devices = EEGDeviceFamily.LookForAllKindsOfDevices();
		if (devices.Count > 0) {
			device = devices[0];
			foreach (var dev in devices)
				Debug.Log("Device found: " + dev.ID);
		} else
			Debug.Log("Cannot find device!");
	}

	public void Connect() {
		if (device != null && device.Connect())
			Debug.Log("Device connected!");
		else
			Debug.Log("Cannot connect to device!");
	}

	public void Quit() {
		if (device != null && device.IsConnected())
			device.Disconnect();
		Application.Quit ();
	}
	
	void Update() {
		if (device != null && device.IsConnected()) {
			Debug.Log("Meditation: " + device["NeuroskyMeditationFeature"].Value);
		}
	}
}
