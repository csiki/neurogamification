using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MuseDeviceFamily : EEGDeviceFamily
{
	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private float defaultHistoryLen = 10f;
	
	public MuseDeviceFamily() : base("Muse") {
		// TODO add available eeg features
		RegisterFeature("MuseAlphaRelativeFeature");
	}
	
	public override List<EEGDevice> LookForDevices() {
		// TODO android side update + multiple device load
		List<EEGDevice> devices = new List<EEGDevice>();
		using (deviceClass = new AndroidJavaClass("com.neurogamification.wrapper.muse.MuseWrapper")) {
			if (deviceClass != null) {
				deviceHandler = deviceClass.CallStatic<AndroidJavaObject> ("getInstance");
				if (deviceHandler != null) {
					if (deviceHandler.Call<bool>("refresh")) {
						for (int id = 0; id < deviceHandler.Call<int>("getNumOfDevices"); ++id) {
							GameObject museGO = new GameObject();
							var deviceComponent = museGO.AddComponent<MuseDevice>();
							deviceComponent.Init(this, defaultHistoryLen, id, deviceClass, deviceHandler);
							devices.Add(deviceComponent);
						}
					}
				} else
					Debug.LogError("Cannot initiate Muse device handler!");
			} else
				Debug.LogError ("Could not load MuseWrapper Java class!");
		}
		return devices;
	}
	
	public override bool IsAccelerationAvailable () { return true; }
}


