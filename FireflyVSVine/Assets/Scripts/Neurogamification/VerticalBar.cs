using UnityEngine;
using System.Collections;

public class VerticalBar : MonoBehaviour
{
	private GameObject indicator;
	public Vector3 indicatorBaseScale = new Vector3(1f, 0f, 0.001f);
	
	public string id;
	public float maxValue;
	public float minValue;
	public float barHeight;
	private bool valueUpdated = false;
	private float value = 0f;
	public float Value {
		get { return value; }
		set { this.value = value; valueUpdated = true; }
	}
	
	void Awake() {
		indicator = GameObject.CreatePrimitive(PrimitiveType.Cube);
		indicator.transform.localScale = indicatorBaseScale;
		indicator.transform.position = transform.position;
	}
	
	void Start () {}
	
	void FixedUpdate() {
		if (valueUpdated) {
			if (value - minValue == 0) {
				indicator.transform.localScale = indicatorBaseScale;
				indicator.transform.position = transform.position;
			}
			else {
				var indicatorPos = new Vector3(transform.position.x,
					transform.position.y + barHeight * ((value - minValue) / (maxValue - minValue)) / 2f,
					transform.position.z);
				
				var indicatorScale = new Vector3(indicator.transform.localScale.x,
				                                 barHeight * ((value - minValue) / (maxValue - minValue)),
				                                 indicator.transform.localScale.z);
				
				indicator.transform.position = indicatorPos;
				indicator.transform.localScale = indicatorScale;
			}
			valueUpdated = false;
		}
	}
	
	void Update () {}
}

