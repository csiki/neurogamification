
using System.Collections.Generic;
using System.Linq;

public abstract class EEGFeature {
	
	private EEGDevice device;
	private string name;
	public string Name {
		get { return name; }
	}
	private float eegHistLenAcquired;
	public float EEGHistSizeAcquired {
		get { return eegHistLenAcquired; }
	}
	private float updatedLastTime = 0;
	private double lastVal = 0;
	protected List<string> dependencies = new List<string>();
	
	protected List<double> valuesSinceLastUpdate = new List<double>();
	public List<double> ValuesSinceLastUpdate {
		get {
			Update();
			return valuesSinceLastUpdate;
		}
	}
	public double Value {
		get {
			Update();
			if (valuesSinceLastUpdate.Count > 0)
				return valuesSinceLastUpdate.Average();
			return lastVal;
		}
	}
	private bool activated = true;
	public bool Activated {
		get { return activated; }
		set { activated = value; }
	}

	public EEGFeature (EEGDevice device, string name, float eegHistLenAcquired) {
		this.device = device;
		this.name = name;
		this.eegHistLenAcquired = eegHistLenAcquired;
		device.IncreaseEEGHistoryLen(eegHistLenAcquired);
	}
	
	public void Update() {
		if (activated) {
			// checks
			if (device.EEGCount == 0 || updatedLastTime == device.EEGLastUpdate)
				return;
			foreach (var featureName in dependencies)
				if (device[featureName].ValuesSinceLastUpdate.Count == 0)
					return;
			
			// update
			if (valuesSinceLastUpdate.Count > 0)
				lastVal = valuesSinceLastUpdate.Last();
			updatedLastTime = device.EEGLastUpdate;
			ForceUpdate();
		}
	}
	
	protected abstract void ForceUpdate();
}

