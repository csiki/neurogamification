﻿using UnityEngine;
using System.Collections.Generic;

public class NeuroskyDeviceFamily : EEGDeviceFamily {

	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private float defaultHistoryLen = 10f;
	
	public NeuroskyDeviceFamily() : base("Neurosky") {
		// TODO add available eeg features
		RegisterFeature("NeuroskyMeditationFeature");
	}
	
	public override List<EEGDevice> LookForDevices() {
		// TODO multiple device load?
		List<EEGDevice> devices = new List<EEGDevice>();
		using (deviceClass = new AndroidJavaClass("com.neurogamification.wrapper.neurosky.NeuroskyWrapper")) {
			if (deviceClass != null) {
				deviceHandler = deviceClass.CallStatic<AndroidJavaObject> ("getInstance");
				if (deviceHandler != null) {
					if (deviceHandler.Call<bool>("isPaired") || deviceHandler.Call<bool>("isFound") // TODO extensive testing
					    || deviceHandler.Call<bool>("isConnected") || true) { // FIXME
						GameObject neuroskyGO = new GameObject();
						var deviceComponent = neuroskyGO.AddComponent<NeuroskyDevice>();
						deviceComponent.Init(this, defaultHistoryLen, 0, deviceClass, deviceHandler);
						devices.Add(deviceComponent);
					}
				} else
					Debug.LogError("Cannot initiate Neurosky device handler!");
			} else
				Debug.LogError ("Could not load MuseWrapper Java class!");
		}
		return devices;
	}
	
	public override bool IsAccelerationAvailable () { return false; }
}
