using System.Collections.Generic;
using System.Linq;

public abstract class EEGDeviceFamily {

	///// CLASS SPECIFIC /////
	private static List<EEGDeviceFamily> registeredFamilies = new List<EEGDeviceFamily>();
	static EEGDeviceFamily() {
		//registeredFamilies.Add(new MuseDeviceFamily());
		registeredFamilies.Add(new NeuroskyDeviceFamily());
		// TODO add families here
	}
	
	public static void RegisterFamily(EEGDeviceFamily family) {
		registeredFamilies.Add (family);
	}
	
	public static List<EEGDevice> LookForAllKindsOfDevices () {
		List<EEGDevice> res = new List<EEGDevice> ();
		foreach (var family in registeredFamilies) {
			res.AddRange(family.LookForDevices());
		}
		return res;
	}

	///// INSTANCE SPECIFIC /////
	private string name;
	public string Name {
		get { return name; }
	}
	protected HashSet<EEGChannel> availableChannels = new HashSet<EEGChannel>();
	public HashSet<EEGChannel> AvailableChannels {
		get {
			return availableChannels;
		}
	}
	
	protected float EEGsamplingFreq;
	public float EEGSamplingFreq {
		get { return EEGsamplingFreq; }
	}
	
	protected float accSamplingFreq;
	public float AccSamplingFreq {
		get { return accSamplingFreq; }
	}
	
	protected HashSet<string> availableEEGFeatures = new HashSet<string>();

	public EEGDeviceFamily (string name) {
		this.name = name;
	}

	public bool IsChannelAvailable(EEGChannel chan) {
		return availableChannels.Contains(chan);
	}

	public bool IsEEGFeatureAvailable(string featureName) {
		return availableEEGFeatures.Contains(featureName);
	}

	public void RegisterFeature(string feature) {
		availableEEGFeatures.Add (feature);
	}
	
	public abstract List<EEGDevice> LookForDevices();
	public abstract bool IsAccelerationAvailable();
}

