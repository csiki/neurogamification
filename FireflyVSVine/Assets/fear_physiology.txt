PHYSIOLOGY OF FEAR (number of similar-opposite reactions):
==================
HR inc 5-1
LVET dec 1
PEP dec 1-1
SV dec 2
CO inc 1
SBP inc 7
DBP inc 6-1
MAP inc 3-1
TPR dec -3
FPA dec 4
FPTT dec 1-2
EPTT dec -2
FT dec 2-1
SCR inc 4-1
nSRR inc 8
SCL inc 6-1
RR inc 5
Te dec 2-1
V(vol) inc 0
pCO2 dec 1-1
alpha-adrenergic inc 3
beta-adrenergic inc 1-2
cholinergi inc 5-1
vagal dec 3-3
respiratory inc 0


COMPETING EMOTIONS:
===================
anger - HR, HRV, LVET, PEP, SBP, DBP, -TPR, FPA, FPTT, FT, SCR, nSRR, SCL, RR, alpha-adrenergic, beta-adrenergic, cholinergi, vagal, respiratory
anxiety - HR, HRV, SBP, DBP, FPA, SCR, nSRR, SCL, RR, Ti, Te, pCO2, cholinergi, vagal, respiratory
disgust - -HRV, SV, SBP, DBP, MAP, -TPR, FPA, SCR, nSRR, SCL, RR, Ti, -Te, alpha-adrenergic, cholinergi, -vagal, respiratory
sadness (anticipatory) - HR, SV, CO, SBP, DBP, -TPR, nSRR, SCL, cholinergi
sadness (acute) - -HR, -PEP, -DBP, -MAP, FPA, -FPTT, -EPTT, FT, -SCR, -SCL, nSRR, -pCO2, -beta-adrenergic, -cholinergi, -vagal, respiratory
amusement - -HRV, SBP, DBP, MAP, SCR, nSRR, SCL, RR, alpha-adrenergic, cholinergi, -vagal, respiratory
happiness - HR, HRV, SBP, DBP, MAP, -FPTT, -EPTT, -FT, nSRR, SCL, RR, Ti, Te, -beta-adrenergic, cholinergi, vagal, respiratory
joy - HR, SBP, nSRR, beta-adrenergic


BEST PHYSILOGY INDICATORS OF FEAR
=================================
TPR: Total Peripherial Resistance = v�rnyom�s + pulzus kisz�molhat�
	- v�rnyom�st pulzushull�mb�l approxim�lni
	- pulzushull�m: pulseoxim�terrel m�rhet� (nagyonnagyon torz)
    - the total resistance to flow of blood in the systemic circuit
    - produced by dividing the mean arterial pressure by the cardiac minute-volume
    - http://www.instructables.com/id/Automated-Blood-Pressure-Cuff-Arduino-Project/
FPA-FPTT: Finger Pulse Amplitude, Finger Pulse Transit Time
    - http://pulsesensor.com/pages/pulse-sensor-amped-arduino-v1dot1
    - http://www.instructables.com/id/LED-Pulse-Sensor-PPG-for-Arduino/&ALLSTEPS
    - http://www.instructables.com/id/Simple-DIY-Pulse-Sensor/
EPTT: Ear Pulse Transit Time
	- pulzus hull�m terjed�s�nek sebess�ge, a v�r�raml�s sebess�ge - egyetlen �t�s id�tartama
    - ELTE openSCR: http://www.affektiv.hu/doku.php?id=openscr
SCL-SCR:
    - measurement locations: http://www.ncbi.nlm.nih.gov/pubmed/22330325
    - tool: https://www.cooking-hacks.com/galvanic-skin-response-sensor-gsr-sweating-ehealth-medical
FT: finger temperature
    - http://www.intorobotics.com/pick-best-temperature-sensor-arduino-project/
    - http://www.danielandrade.net/2008/07/05/temperature-sensor-arduino/

Mellkas m�r�s (giroszk�p) - l�gz�s �teme !!!
B�relln�ll�s izom felett m�rni
Hiperventill�ci�: pulzoxim�ter magasabb lesz az oxig�n ar�nya
	- http://www.meas-spec.com/photo-optic-sensors/photo-optic-components.aspx
Olyan f�met haszn�lj ami nem v�lt ki allergi�s reakci�t
F�lelem --> adrenalin --> menek�l�shez fontos szervek kapnak v�rt --> ujj nem kap --> hideg lesz

http://www.sono-tek.com/selectaflux/
http://www.digikey.com/product-search/en/sensors-transducers/optical-sensors-photo-detectors-logic-output/1967050
http://www.medline.com/product/Skin-Temperature-Sensors/Z05-PF32150

l�gz�ssz�m (mellkas), pulzussz�m (csukl�), ujjh�m�rs�klet (gy�r�), b�rellen�ll�s (h�velykujj alatt)