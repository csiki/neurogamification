﻿using UnityEngine;
using System.Collections.Generic;

public class NeuroskyDevice : EEGDevice {
	
	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private int neuroskyID = 0;
	private double voltMultiplier = (1800.0 / (4096.0 * 2.0)); // to microvolt
	
	public void Init(EEGDeviceFamily family, float eegHistoryLen, int neuroskyID, AndroidJavaClass deviceClass, AndroidJavaObject deviceHandler) {
		InitDevice(family.Name + neuroskyID, family, eegHistoryLen);
		this.neuroskyID = neuroskyID;
		this.deviceClass = deviceClass;
		this.deviceHandler = deviceHandler;
		
		// TODO add neurosky specific features
		RegisterFeature(new NeuroskyMeditationFeature(this, deviceHandler), false);
		RegisterFeature(new NeuroskyAttentionFeature(this, deviceHandler), false);
	}
	
	// CONNECTION
	public override bool Connect() { // TODO ??? how to handle return value
		if (deviceHandler != null)
			deviceHandler.Call("tryConnect");
		return true;
	}
	public override void Disconnect() {
		deviceHandler.Call("close");
	}
	public override bool IsConnected() {
		try {
			return deviceHandler.Call<bool>("isConnected");
		} catch {}
		return false;
	}
	public override float SignalQuality () {
		return 1f - deviceHandler.Call<int>("getPoorSignal") / 100f;
	}
	
	// RETREIVER METHODS
	protected override void RetreiveEEG() {
		var dict = new Dictionary<EEGChannel, double>();
		for (int i = 0; i < deviceHandler.Call<int>("getEegNum"); ++i) {
			dict[EEGChannel.Fp1] = deviceHandler.Call<int>("pollEEG") * voltMultiplier;
			eeg.AddLast(new KeyValuePair<float, Dictionary<EEGChannel, double>>(
				(float)deviceHandler.Call<long>("pollTime"), dict));
		}
	}
	protected override void RetreiveAcceleration() {}
	
	// MONOBEHAVIOR METHODS
	protected override void Start () {}
	protected override void Update() {}
}
