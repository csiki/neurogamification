using UnityEngine;
using System.Collections.Generic;
using System;

public class NeuroskyAttentionFeature : EEGFeature
{
	private AndroidJavaObject deviceHandler = null;
	
	public NeuroskyAttentionFeature(EEGDevice device, AndroidJavaObject deviceHandler) : base(device, "NeuroskyMeditationFeature", 0f) {
		if (device.Family.Name != "Neurosky")
			throw new ArgumentException("Neurosky eSense attention only works with a Neurosky device!");
		this.deviceHandler = deviceHandler;
	}
	
	protected override void ForceUpdate() {
		var curratt = deviceHandler.Call<int>("getAttention");
		if (valuesSinceLastUpdate.Count > 0 && valuesSinceLastUpdate[0] == curratt)
			return;
		valuesSinceLastUpdate.Clear();
		valuesSinceLastUpdate.Add(curratt);
	}
}

