using UnityEngine;
using System.Collections.Generic;
using System;

public class NeuroskyMeditationFeature : EEGFeature
{
	private AndroidJavaObject deviceHandler = null;

	public NeuroskyMeditationFeature(EEGDevice device, AndroidJavaObject deviceHandler) : base(device, "NeuroskyMeditationFeature", 0f) {
		if (device.Family.Name != "Neurosky")
			throw new ArgumentException("Neurosky eSense attention only works with a Neurosky device!");
		this.deviceHandler = deviceHandler;
	}
	
	protected override void ForceUpdate() {
		var currmed = deviceHandler.Call<int>("getMeditation");
		if (valuesSinceLastUpdate.Count > 0 && valuesSinceLastUpdate[0] == currmed)
			return;
		valuesSinceLastUpdate.Clear();
		valuesSinceLastUpdate.Add(currmed);
	}
}

