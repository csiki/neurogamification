using UnityEngine;
using System.Collections.Generic;
using System;

public class MuseFrontalAlphaRelativeFeature : EEGFeature
{
	private int museID = 0;
	private AndroidJavaObject deviceHandler = null;
	
	public MuseFrontalAlphaRelativeFeature(EEGDevice device, AndroidJavaObject deviceHandler, int museID) : base(device, "MuseFrontalAlphaRelativeFeature", 0f) {
		if (device.Family.Name != "Muse")
			throw new ArgumentException("MuseFrontalAlphaRelativeFeature only works with a Muse device!");
		this.deviceHandler = deviceHandler;
		this.museID = museID;
	}
	
	protected override void ForceUpdate() {
		valuesSinceLastUpdate.Clear();
		var arPacket = deviceHandler.Call<AndroidJavaObject>("pollAlphaRelative", museID);
		while (arPacket.Get<bool>("valid")) {
			var fp1 = arPacket.Get<double>("FP1");
			var fp2 = arPacket.Get<double>("FP2");
			double sum = 0;
			int count = 0;
			if (fp1 == fp1) { ++count; sum += fp1; }
			if (fp2 == fp2) { ++count; sum += fp2; }
			if (count > 0)
				valuesSinceLastUpdate.Add(sum / count);
			arPacket = deviceHandler.Call<AndroidJavaObject>("pollAlphaRelative", museID);
		}
	}
}

