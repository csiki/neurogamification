using UnityEngine;
using System.Collections.Generic;

public class MuseDevice : EEGDevice {
	
	private int museID = 0;
	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject deviceHandler = null;
	private bool connected = false;
	
	public void Init(EEGDeviceFamily family, float eegHistoryLen, int museID, AndroidJavaClass deviceClass, AndroidJavaObject deviceHandler) {
		InitDevice(family.Name + museID, family, eegHistoryLen);
		this.museID = museID;
		this.deviceClass = deviceClass;
		this.deviceHandler = deviceHandler;
		
		// TODO add muse specific features
		RegisterFeature(new MuseFrontalAlphaRelativeFeature(this, deviceHandler, museID), false);
	}
	
	// CONNECTION
	public override bool Connect() { // TODO repair android side at connect() !!!
		if (deviceHandler != null) {
			bool isthereany = deviceHandler.Call<bool>("refresh");
			if (isthereany && deviceHandler.Call<bool>("connect", museID)) {
				connected = true;
				return true;
			}
		}
		return false;
	}
	public override void Disconnect() {
		deviceHandler.Call("disconnect", museID);
		connected = false;
	}
	public override bool IsConnected() {
		return connected;
	}
	public override float SignalQuality ()
	{
		// TODO
		return 1f;
	}
	
	// RETREIVER METHODS
	protected override void RetreiveEEG() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG", museID);
		if (eegPacket != null) {
			while (eegPacket.Get<bool> ("valid")) {
				packets.Add(eegPacket);
				eegPacket = deviceHandler.Call<AndroidJavaObject>("pollEEG", museID);
			}
			foreach (var packet in packets) {
				var dict = new Dictionary<EEGChannel, double>();
				dict.Add(EEGChannel.TP9, packet.Get<double>("TP9"));
				dict.Add(EEGChannel.TP10, packet.Get<double>("TP10"));
				dict.Add(EEGChannel.Fp1, packet.Get<double>("FP1"));
				dict.Add(EEGChannel.Fp2, packet.Get<double>("FP2"));
				eeg.AddLast(new KeyValuePair<float, Dictionary<EEGChannel, double>>(packet.Get<long>("timestamp") / 1000f, dict)); // converted to milliseconds
			}
		}
	}
	protected override void RetreiveAcceleration() {
		List<AndroidJavaObject> packets = new List<AndroidJavaObject>();
		var accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer", museID);
		while (accPacket.Get<bool> ("valid")) {
			packets.Add(accPacket);
			accPacket = deviceHandler.Call<AndroidJavaObject>("pollAccelerometer", museID);
		}
		AccelerometerData prev = null;
		if (acc.Count > 0) prev = acc.Last.Value.Value;
		foreach (var packet in packets) {
			AccelerometerData accdata = null;
			if (prev != null)
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"), prev);
			else
				accdata = new AccelerometerData(packet.Get<double>("ForBack"), packet.Get<double>("UpDown"), packet.Get<double>("LR"));
			
			acc.AddLast(new KeyValuePair<float, AccelerometerData>(packet.Get<long>("timestamp") / 1000f, accdata)); // converted to milliseconds
			prev = accdata;
		}
	}
	
	// MONOBEHAVIOR METHODS
	protected override void Start () {}
	protected override void Update() {}
	
}

