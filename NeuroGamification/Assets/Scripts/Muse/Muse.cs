﻿using UnityEngine;
using System.Collections;

// TODO support multiple devices
public class Muse : MonoBehaviour {

	private AndroidJavaClass deviceClass = null;
	private AndroidJavaObject device = null;
	private bool connected = false;

	public void Refresh() {
		//if (device == null) {
			using (deviceClass = new AndroidJavaClass("com.neurogamification.wrapper.muse.MuseWrapper")) {
				if (deviceClass != null) {
					Debug.Log ("Device class loaded!");
					device = deviceClass.CallStatic<AndroidJavaObject> ("getInstance");
					if (device != null) {
						Debug.Log("Device variable initiated!");
						bool isthereany = device.Call<bool>("refresh");
						if (isthereany) {
							Debug.Log("Muse device found!");
							// TODO list them
						}
					} else
						Debug.LogError("Cannot initiate device!");
				} else
					Debug.LogError ("Could not load MuseWrapper Java class!");
			}
		//}
	}

	public void Connect(int to) {
		if (device != null) {
			bool isthereany = device.Call<bool>("refresh");
			if (isthereany) {
				Debug.Log("Muse device found!");
				// TODO list them
				connected = device.Call<bool>("connect", to);
				if (connected) {
					Debug.Log("Muse device connected!");
				} else
					Debug.LogError("Cannot connect to Muse device!");
			} else
				Debug.LogError("No Muse device found!");
		} else
			Debug.LogError ("Cannot connect, no device handler is attached!");
	}

	// Use this for initialization
	void Start () {
		if (device == null) {
			using (deviceClass = new AndroidJavaClass("com.neurogamification.wrapper.muse.MuseWrapper")) {
				if (deviceClass != null) {
					device = deviceClass.CallStatic<AndroidJavaObject>("getInstance");
					bool isthereany = device.Call<bool>("refresh");
					if (isthereany) {
						connected = device.Call<bool>("connect", 0);
						if (connected) {
							Debug.Log("Muse device connected!");
						} else
							Debug.LogError("Cannot connect to Muse device!");
					} else
						Debug.LogError("No Muse device found!");
				} else
					Debug.LogError("Could not load MuseWrapper Java class!");
			}
		}
		Time.fixedDeltaTime = 0.5f;
	}

	void FixedUpdate() {

		if (device == null) return;
		var EEGpacket = device.Call<AndroidJavaObject>("pollEEG");
		var EEGARpacket = device.Call<AndroidJavaObject>("pollAlphaRelative");
		var ACCpacket = device.Call<AndroidJavaObject>("pollAccelerometer");
		var ndivece = device.Call<int> ("getNumOfDevices");
		var connState = device.Call<string> ("getConnectionState");
		var headbandOn = device.Call<bool> ("isHeadbandOn");
		var blinked = device.Call<bool> ("blinked");
		var jawClenched = device.Call<bool> ("jawClenched");
		Debug.Log ("DEVICE NUM: " + ndivece.ToString());
		Debug.Log ("DEVICE STATE: " + connState);
		Debug.Log ("Headband on: " + headbandOn);
		Debug.Log ("Blinked: " + blinked);
		Debug.Log ("Jaw clenched: " + jawClenched);

		if (EEGpacket.Get<bool> ("valid")) {
			var TP9 = EEGpacket.Get<double>("TP9");
			var FP1 = EEGpacket.Get<double>("FP1");
			var FP2 = EEGpacket.Get<double>("FP2");
			var TP10 = EEGpacket.Get<double>("TP10");
			Debug.Log("RAW EEG: TP9: " + TP9 + "; FP1: " + FP1 + "; FP2: " + FP2 + "; TP10: " + TP10);
		} else
			Debug.Log ("No EEG recording since last poll.");
		if (EEGARpacket.Get<bool> ("valid")) {
			var TP9ar = EEGpacket.Get<double>("TP9");
			var FP1ar = EEGpacket.Get<double>("FP1");
			var FP2ar = EEGpacket.Get<double>("FP2");
			var TP10ar = EEGpacket.Get<double>("TP10");
			Debug.Log("ALPHA RELATIVE: TP9: " + TP9ar + "; FP1: " + FP1ar + "; FP2: " + FP2ar + "; TP10: " + TP10ar);
		} else
			Debug.Log ("No alpha relative recording since last poll.");
		if (ACCpacket.Get<bool> ("valid")) {
			var ForBack = ACCpacket.Get<double>("ForBack");
			var UpDown = ACCpacket.Get<double>("UpDown");
			var LR = ACCpacket.Get<double>("LR");
			Debug.Log("ACC meter: for-back: " + ForBack + "; up-down: " + UpDown + "; left-right: " + LR);
		} else
			Debug.Log ("No ACC recording since last poll.");
	}

	void Update () {}

}
