using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class EEGDevice : MonoBehaviour {

	private string id;
	public string ID { get { return id; } }
	protected EEGDeviceFamily family;
	public EEGDeviceFamily Family { get { return family; } }
	protected float lastEEGPoll;
	protected float lastAccPoll;
	protected float eegHistoryLen; // in sec
	protected float accHistoryLen; // in sec
	protected LinkedList<KeyValuePair<float, Dictionary<EEGChannel, double>>> eeg;
	protected LinkedList<KeyValuePair<float, AccelerometerData>> acc;
	protected Dictionary<string, EEGFeature> eegFeatures;
	
	public void InitDevice(string id, EEGDeviceFamily family, float eegHistoryLen) {
		this.id = id;
		this.family = family;
		this.eegHistoryLen = eegHistoryLen;
		this.accHistoryLen = 5f;
		lastEEGPoll = 0f;
		lastAccPoll = 0f;
		eeg = new LinkedList<KeyValuePair<float, Dictionary<EEGChannel, double>>>();
		acc = new LinkedList<KeyValuePair<float, AccelerometerData>>();
		eegFeatures = new Dictionary<string, EEGFeature>();
	}

	// SIMPLIFIED DATA ACQUISITION
	public LinkedList<KeyValuePair<float, Dictionary<EEGChannel, double>>> EEGHistory {
		get { return eeg; }
	}
	public double this [EEGChannel chan] {
		get { return GetEEG(chan); }
	}
	public EEGFeature this [string featureName] {
		get {
			if (eegFeatures.ContainsKey(featureName))
				return eegFeatures[featureName];
			return null;
		}
	}
	public Dictionary<EEGChannel, double> EEG {
		get {
			if (eeg.Count > 0)
				return eeg.Last.Value.Value;
			return new Dictionary<EEGChannel, double>();
		}
	}
	public float EEGLastUpdate {
		get {
			if (eeg.Count > 0)
				return eeg.Last.Value.Key;
			return 0f;
		}
	}
	public float AccelerationLastUpdate {
		get {
			if (acc.Count > 0)
				return acc.Last.Value.Key;
			return 0f;
		}
	}
	public AccelerometerData Acceleration {
		get {
			if (acc.Count > 0)
				return acc.Last.Value.Value;
			return new AccelerometerData(0, 0, 0);
		}
	}
	public int EEGCount {
		get { return eeg.Count; }
	}
	public int AccelerationCount {
		get { return acc.Count; }
	}

	// SETTER FUNCTIONS
	public void IncreaseEEGHistoryLen(float minHistLen) {
		eegHistoryLen = minHistLen > eegHistoryLen ? minHistLen : eegHistoryLen;
	}

	// EEG RETRIEVER FUNCTIONS
	public double GetEEG(EEGChannel fromChan, bool countAsPoll) {
		if (!family.IsChannelAvailable (fromChan))
			throw new ArgumentException (fromChan.ToString());
		if (eeg.Count > 0) {
			var lastEEG = eeg.Last.Value;
			if (countAsPoll) lastAccPoll = lastEEG.Key;
			return lastEEG.Value[fromChan];
		}
		return 0;
	}
	public List<double> GetEEG(List<EEGChannel> fromChans, bool countAsPoll) {
		if (eeg.Count == 0) return new List<double>();
		var lastEEG = eeg.Last.Value;
		var eegData = new List<double> (fromChans.Count);
		foreach (var chan in fromChans) {
			if (!family.IsChannelAvailable (chan))
				throw new ArgumentException (chan.ToString());
			eegData.Add(lastEEG.Value[chan]);
		}
		if (countAsPoll) lastAccPoll = lastEEG.Key;
		return eegData;
	}
	public List<KeyValuePair<float, double>> GetEEGSinceLastPoll(EEGChannel fromChan, bool countAsPoll) {
		if (!family.IsChannelAvailable (fromChan))
			throw new ArgumentException (fromChan.ToString());
		if (eeg.Count == 0) return new List<KeyValuePair<float, double>>();
		var result = new List<KeyValuePair<float, double>> ();
		foreach (var eegRecord in eeg) {
			if (eegRecord.Key > lastEEGPoll)
				result.Add(new KeyValuePair<float, double>(eegRecord.Key, eegRecord.Value[fromChan]));
		}
		if (countAsPoll) lastAccPoll = eeg.Last.Value.Key;
		return result;
	}
	public List<KeyValuePair<float,List<double>>> GetEEGSinceLastPoll(List<EEGChannel> fromChans, bool countAsPoll) {
		if (eeg.Count == 0) return new List<KeyValuePair<float,List<double>>>();
		var result = new List<KeyValuePair<float,List<double>>> ();
		foreach (var eegRecord in eeg) {
			if (eegRecord.Key > lastEEGPoll) {
				var tmpRec = new List<double>();
				foreach (var chan in fromChans) {
					if (!family.IsChannelAvailable (chan))
						throw new ArgumentException (chan.ToString());
					tmpRec.Add(eegRecord.Value[chan]);
				}
				result.Add(new KeyValuePair<float, List<double>>(eegRecord.Key, tmpRec));
			}
		}
		if (countAsPoll) lastEEGPoll = eeg.Last.Value.Key;
		return result;
	}

	// ACCELEROMETER RETRIEVER FUNCTIONS
	public bool IsAccelerationAvailable() { return family.IsAccelerationAvailable(); }
	public AccelerometerData GetAcceleration(bool countAsPoll) {
		if (acc.Count > 0)
			return acc.Last.Value.Value;
		return new AccelerometerData (0, 0, 0);
	}
	public List<KeyValuePair<float, AccelerometerData>> GetAccelerationSinceLastPoll(bool countAsPoll) {
		if (acc.Count == 0)
			return new List<KeyValuePair<float, AccelerometerData>>();
		var result = new List<KeyValuePair<float, AccelerometerData>>();
		foreach (var accRecord in acc) {
			if (accRecord.Key > lastAccPoll)
				result.Add(accRecord);
		}
		if (countAsPoll) lastAccPoll = acc.Last.Value.Key;
		return result;
	}
	
	// FEATURE RELATED FUNCTIONS
	// virtual: e.g. Muse has a feature of already calculate relative alpha this function just returns that if asked
	public virtual double FeatureValue(string featureName) {
		return eegFeatures[featureName].Value;
	}
	public bool RegisterFeature(EEGFeature feature) {
		if (family.IsEEGFeatureAvailable(feature.Name)) {
			if (!eegFeatures.ContainsKey(feature.Name))
				eegFeatures.Add(feature.Name, feature);
			return true;
		}
		return false; // TODO throw exception?!4
	}
	public bool RegisterFeature(EEGFeature feature, bool activate) {
		feature.Activated = activate;
		return RegisterFeature(feature);
	}
	public bool ActivateFeature(string featureName) {
		return ActivateFeature(featureName, true);
	}
	public bool ActivateFeature(string featureName, bool activate) {
		if (eegFeatures.ContainsKey(featureName)) {
			eegFeatures[featureName].Activated = activate;
			return true;
		}
		return false;
	}

	// HELPER FUNCTIONS (default parameters)
	public double GetEEG(EEGChannel fromChan) {
		return GetEEG (fromChan, false);
	}
	public List<double> GetEEG(List<EEGChannel> fromChans) {
		return GetEEG (fromChans, false);
	}
	public List<KeyValuePair<float, double>> GetEEGSinceLastPoll(EEGChannel fromChan) {
		return GetEEGSinceLastPoll (fromChan, true);
	}
	public List<KeyValuePair<float,List<double>>> GetEEGSinceLastPoll(List<EEGChannel> fromChans) {
		return GetEEGSinceLastPoll(fromChans, true);
	}
	public AccelerometerData GetAcc() {
		return GetAcceleration(false);
	}
	public List<KeyValuePair<float, AccelerometerData>> GetAccelerationSinceLastPoll() {
		return GetAccelerationSinceLastPoll(true);
	}

	// CONNECTION
	public abstract bool Connect();
	public abstract void Disconnect();
	public abstract bool IsConnected();
	public abstract float SignalQuality();

	// RETREIVER METHODS
	protected abstract void RetreiveEEG();
	protected abstract void RetreiveAcceleration();
	
	// MONOBEHAVIOR METHODS
	protected virtual void Start () {}
	protected virtual void Update() {}
	protected virtual void FixedUpdate() {
		if (IsConnected()) {
			RetreiveEEG ();
			RetreiveAcceleration ();
			foreach (var feature in eegFeatures)
				feature.Value.Update();
	
			if (eeg.Count > 0)
				while (eeg.Last.Value.Key - eeg.First.Value.Key > eegHistoryLen)
					eeg.RemoveFirst ();
	
			if (acc.Count > 0)
				while (acc.Last.Value.Key - acc.First.Value.Key > accHistoryLen)
					acc.RemoveFirst ();
		}
	}
}

