using UnityEngine;

public class AccelerometerData
{
	public double x, y, z;
	public Vector3 speed;

	public AccelerometerData (double x, double y, double z, AccelerometerData prev) {
		this.x = x;
		this.y = y;
		this.z = z;
		speed = new Vector3();
		speed = CalcSpeed(this, prev);
	}
	
	public AccelerometerData (double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		speed = new Vector3();
	}
	
	public static Vector3 CalcSpeed(AccelerometerData current, AccelerometerData previous) {
		// TODO is this function necessary?
		return new Vector3();
	}
}


