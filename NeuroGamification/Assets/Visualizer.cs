﻿using UnityEngine;
using System.Collections;

public class Visualizer : MonoBehaviour {
	
	public GameObject alphaBarGO;
	private VerticalBar alphaBar;
	private EEGDevice device = null;
	
	void Start () {
		alphaBar = alphaBarGO.GetComponent<VerticalBar>();
		Debug.Log("fuckdatshit");
	}
	
	void Update () {
		if (device == null) {
			var devices = EEGDeviceFamily.LookForAllKindsOfDevices();
			if (devices.Count > 0) {
				device = devices[0];
				if (device.Connect()) {
					Debug.Log("Connected to: " + device.ID);
					if (device.Family.Name == "Muse") {
						device.ActivateFeature("MuseAlphaRelativeFeature");
						Debug.Log("MuseAlphaRelativeFeature feature activated!");
					}
				}
				else
					Debug.Log("Cannot connect to: " + device.ID);
			}
		} else if (alphaBar != null) {
			alphaBar.Value = (float)device["MuseFrontalAlphaRelativeFeature"].Value;
			Debug.Log("AR size: " + device["MuseFrontalAlphaRelativeFeature"].ValuesSinceLastUpdate.Count);
			Debug.Log("AR: " + device["MuseFrontalAlphaRelativeFeature"].Value);
			Debug.Log("EEG: " + device.EEG[EEGChannel.Fp1]);
		}
	}
}
