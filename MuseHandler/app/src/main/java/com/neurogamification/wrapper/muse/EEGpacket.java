package com.neurogamification.wrapper.muse;

public class EEGpacket {

    public final boolean valid;
    public final long timestamp;
    public final double TP9;
    public final double FP1;
    public final double FP2;
    public final double TP10;

    public EEGpacket() {
        valid = false;
        timestamp = 0;
        this.TP9 = 0;
        this.FP1 = 0;
        this.FP2 = 0;
        this.TP10 = 0;
    }

    public EEGpacket(long timestamp, double TP9, double FP1, double FP2, double TP10) {
        valid = true;
        this.timestamp = timestamp;
        this.TP9 = TP9;
        this.FP1 = FP1;
        this.FP2 = FP2;
        this.TP10 = TP10;
    }
}
