package com.neurogamification.wrapper.muse;

public class AccPacket {

    public final boolean valid;
    public final long timestamp;
    public final double ForBack;
    public final double UpDown;
    public final double LR;

    public AccPacket() {
        valid = false;
        timestamp = 0;
        this.ForBack = 0;
        this.UpDown = 0;
        this.LR = 0;
    }

    public AccPacket(long timestamp, double ForBack, double UpDown, double LR) {
        valid = true;
        this.timestamp = timestamp;
        this.ForBack = ForBack;
        this.UpDown = UpDown;
        this.LR = LR;
    }
}
