package com.neurogamification.wrapper.muse;

import com.interaxon.libmuse.ConnectionState;
import com.interaxon.libmuse.MuseConnectionListener;
import com.interaxon.libmuse.MuseConnectionPacket;

class ConnectionListener extends MuseConnectionListener {

    ConnectionState current = null;
    String status = null;

    @Override
    public void receiveMuseConnectionPacket(MuseConnectionPacket p) {
        current = p.getCurrentConnectionState();
        status = p.getPreviousConnectionState().toString() +
                " -> " + current;
        //final String full = "Muse " + p.getSource().getMacAddress() +
        //        " " + status;
    }

    public ConnectionState getCurrentState() {
        return current;
    }

    public String getStatus() {
        return status;
    }
}