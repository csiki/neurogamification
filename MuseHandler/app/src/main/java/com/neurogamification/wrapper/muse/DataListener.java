package com.neurogamification.wrapper.muse;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.interaxon.libmuse.Accelerometer;
import com.interaxon.libmuse.Eeg;
import com.interaxon.libmuse.MuseArtifactPacket;
import com.interaxon.libmuse.MuseDataListener;
import com.interaxon.libmuse.MuseDataPacket;

class DataListener extends MuseDataListener {

    private int EEGHistSize = 0;
    private int spectralHistSize = 0;
    private int AccHistSize = 0;

    private Queue<EEGpacket> eeg, aa, ar, as, ba, br, bs, da, dr, ds, ga, gr, gs, ta, tr, ts;
    private Queue<Double> ForBackAcc;
    private Queue<Double> UpDownAcc;
    private Queue<Double> LRAcc;
    private volatile long currentTimeStamp = 0; // in microseconds since 1970
    private long lastBlink = 0;
    private long lastJawClench = 0;
    private boolean headbandOn = false;
    private double battery = 0;
    private EEGpacket horseshoe = new EEGpacket();

    private long happenedNowThreshold = 1000000; // in microseconds (1 sec)
    private boolean alwaysShowLastUpdate = true;

    public DataListener(int EEGHistSize, int spectralHistSize, int AccHistSize) {
        this.EEGHistSize = EEGHistSize;
        this.spectralHistSize = spectralHistSize;
        this.AccHistSize = AccHistSize;

        eeg = new ConcurrentLinkedQueue<EEGpacket>();

        aa = new ConcurrentLinkedQueue<EEGpacket>();
        ar = new ConcurrentLinkedQueue<EEGpacket>();
        as = new ConcurrentLinkedQueue<EEGpacket>();

        ba = new ConcurrentLinkedQueue<EEGpacket>();
        br = new ConcurrentLinkedQueue<EEGpacket>();
        bs = new ConcurrentLinkedQueue<EEGpacket>();

        da = new ConcurrentLinkedQueue<EEGpacket>();
        dr = new ConcurrentLinkedQueue<EEGpacket>();
        ds = new ConcurrentLinkedQueue<EEGpacket>();

        ga = new ConcurrentLinkedQueue<EEGpacket>();
        gr = new ConcurrentLinkedQueue<EEGpacket>();
        gs = new ConcurrentLinkedQueue<EEGpacket>();

        ta = new ConcurrentLinkedQueue<EEGpacket>();
        tr = new ConcurrentLinkedQueue<EEGpacket>();
        ts = new ConcurrentLinkedQueue<EEGpacket>();

        ForBackAcc = new ConcurrentLinkedQueue<Double>();
        UpDownAcc = new ConcurrentLinkedQueue<Double>();
        LRAcc = new ConcurrentLinkedQueue<Double>();
    }

    public void setHistoryParams(int EEGHistSize, int spectralHistSize, int AccHistSize) {
        this.EEGHistSize = EEGHistSize;
        this.spectralHistSize = spectralHistSize;
        this.AccHistSize = AccHistSize;
    }

    public EEGpacket pollEEGpacket() {
        if (!eeg.isEmpty())
            return eeg.poll();
        return new EEGpacket();
    }

    public EEGpacket pollEEGAlphaAbsolutePacket() {
        if (!aa.isEmpty())
            return aa.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGAlphaRelativePacket() {
        if (!ar.isEmpty())
            return ar.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGAlphaScorePacket() {
        if (!as.isEmpty())
            return as.poll();
        return new EEGpacket();
    }

    public EEGpacket pollEEGBetaAbsolutePacket() {
        if (!ba.isEmpty())
            return ba.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGBetaRelativePacket() {
        if (!br.isEmpty())
            return br.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGBetaScorePacket() {
        if (!bs.isEmpty())
            return bs.poll();
        return new EEGpacket();
    }

    public EEGpacket pollEEGDeltaAbsolutePacket() {
        if (!da.isEmpty())
            return da.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGDeltaRelativePacket() {
        if (!dr.isEmpty())
            return dr.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGDeltaScorePacket() {
        if (!ds.isEmpty())
            return ds.poll();
        return new EEGpacket();
    }

    public EEGpacket pollEEGGammaAbsolutePacket() {
        if (!ga.isEmpty())
            return ga.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGGammaRelativePacket() {
        if (!gr.isEmpty())
            return gr.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGGammaScorePacket() {
        if (!gs.isEmpty())
            return gs.poll();
        return new EEGpacket();
    }

    public EEGpacket pollEEGThetaAbsolutePacket() {
        if (!ta.isEmpty())
            return ta.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGThetaRelativePacket() {
        if (!tr.isEmpty())
            return tr.poll();
        return new EEGpacket();
    }
    public EEGpacket pollEEGThetaScorePacket() {
        if (!ts.isEmpty())
            return ts.poll();
        return new EEGpacket();
    }

    public AccPacket pollAccelerometer() {
        if (!ForBackAcc.isEmpty())
            return new AccPacket(currentTimeStamp, ForBackAcc.poll(), UpDownAcc.poll(), LRAcc.poll());
        return new AccPacket();
    }

    public double getBattery() {
        return battery;
    }

    public EEGpacket getHorseshoe() {
        return horseshoe;
    }

    public long getCurrentTimeStamp() {
        return currentTimeStamp;
    }

    public boolean blinked() {
        return currentTimeStamp - lastBlink < happenedNowThreshold;
    }

    public boolean jawClenched() {
        return currentTimeStamp - lastJawClench < happenedNowThreshold;
    }

    public boolean isHeadbandOn() {
        return headbandOn;
    }

    @Override
    public void receiveMuseDataPacket(MuseDataPacket p) {
        currentTimeStamp = p.getTimestamp();
        switch (p.getPacketType()) {
            case EEG:
                updateEeg(p.getValues());
                break;
            case ACCELEROMETER:
                updateAccelerometer(p.getValues());
                break;
            case ALPHA_RELATIVE:
                updateAlphaRelative(p.getValues());
                break;
            case ALPHA_ABSOLUTE:
                updateAlphaAbsolute(p.getValues());
                break;
            case ALPHA_SCORE:
                updateAlphaScore(p.getValues());
                break;

            case BETA_ABSOLUTE:
                updateBetaAbsolute(p.getValues());
                break;
            case BETA_RELATIVE:
                updateBetaRelative(p.getValues());
                break;
            case BETA_SCORE:
                updateBetaScore(p.getValues());
                break;

            case DELTA_ABSOLUTE:
                updateDeltaAbsolute(p.getValues());
                break;
            case DELTA_RELATIVE:
                updateDeltaRelative(p.getValues());
                break;
            case DELTA_SCORE:
                updateDeltaScore(p.getValues());
                break;

            case GAMMA_ABSOLUTE:
                updateGammaAbsolute(p.getValues());
                break;
            case GAMMA_RELATIVE:
                updateGammaRelative(p.getValues());
                break;
            case GAMMA_SCORE:
                updateGammaScore(p.getValues());
                break;

            case THETA_ABSOLUTE:
                updateThetaAbsolute(p.getValues());
                break;
            case THETA_RELATIVE:
                updateThetaRelative(p.getValues());
                break;
            case THETA_SCORE:
                updateThetaScore(p.getValues());
                break;

            case BATTERY:
                battery = p.getValues().get(2); // TODO right index?
                break;
            case HORSESHOE:
                horseshoe = new EEGpacket(currentTimeStamp, p.getValues().get(Eeg.TP9.ordinal()),
                        p.getValues().get(Eeg.FP1.ordinal()), p.getValues().get(Eeg.FP2.ordinal()),
                        p.getValues().get(Eeg.TP10.ordinal()));  // TODO right indices?
                break;
            default:
                break;
        }
    }

    @Override
    public void receiveMuseArtifactPacket(MuseArtifactPacket p) {
        headbandOn = p.getHeadbandOn();
        if (headbandOn) {
            if (p.getBlink()) lastBlink = currentTimeStamp;
            if (p.getJawClench()) lastJawClench = currentTimeStamp;
        }
    }

    private void updateAccelerometer(final ArrayList<Double> data) {
        ForBackAcc.add(data.get(Accelerometer.FORWARD_BACKWARD.ordinal()));
        UpDownAcc.add(data.get(Accelerometer.UP_DOWN.ordinal()));
        LRAcc.add(data.get(Accelerometer.LEFT_RIGHT.ordinal()));

        if (ForBackAcc.size() > AccHistSize) {
            ForBackAcc.poll();
            UpDownAcc.poll();
            LRAcc.poll();
        }
    }

    private void updateEeg(final ArrayList<Double> data) {
        eeg.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (eeg.size() > EEGHistSize)
            eeg.poll();
    }

    private void updateAlphaAbsolute(final ArrayList<Double> data) {
        aa.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (aa.size() > spectralHistSize)
            aa.poll();
    }

    private void updateAlphaRelative(final ArrayList<Double> data) {
        ar.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ar.size() > spectralHistSize)
            ar.poll();
    }

    private void updateAlphaScore(final ArrayList<Double> data) {
        as.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (as.size() > spectralHistSize)
            as.poll();
    }

    private void updateBetaAbsolute(final ArrayList<Double> data) {
        ba.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ba.size() > spectralHistSize)
            ba.poll();
    }

    private void updateBetaRelative(final ArrayList<Double> data) {
        br.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (br.size() > spectralHistSize)
            br.poll();
    }

    private void updateBetaScore(final ArrayList<Double> data) {
        bs.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (bs.size() > spectralHistSize)
            bs.poll();
    }

    private void updateDeltaAbsolute(final ArrayList<Double> data) {
        da.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (da.size() > spectralHistSize)
            da.poll();
    }

    private void updateDeltaRelative(final ArrayList<Double> data) {
        dr.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (dr.size() > spectralHistSize)
            dr.poll();
    }

    private void updateDeltaScore(final ArrayList<Double> data) {
        as.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ds.size() > spectralHistSize)
            ds.poll();
    }

    private void updateGammaAbsolute(final ArrayList<Double> data) {
        ga.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ga.size() > spectralHistSize)
            ga.poll();
    }

    private void updateGammaRelative(final ArrayList<Double> data) {
        gr.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (gr.size() > spectralHistSize)
            gr.poll();
    }

    private void updateGammaScore(final ArrayList<Double> data) {
        gs.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (gs.size() > spectralHistSize)
            gs.poll();
    }

    private void updateThetaAbsolute(final ArrayList<Double> data) {
        ta.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ta.size() > spectralHistSize)
            ta.poll();
    }

    private void updateThetaRelative(final ArrayList<Double> data) {
        tr.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (tr.size() > spectralHistSize)
            tr.poll();
    }

    private void updateThetaScore(final ArrayList<Double> data) {
        ts.add(new EEGpacket(currentTimeStamp, data.get(Eeg.TP9.ordinal()), data.get(Eeg.FP1.ordinal()),
                data.get(Eeg.FP2.ordinal()), data.get(Eeg.TP10.ordinal())));

        if (ts.size() > spectralHistSize)
            ts.poll();
    }
}