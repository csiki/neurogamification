package com.neurogamification.wrapper.muse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.interaxon.libmuse.*;

// TODO be able to get the last data, and also this wrapper (or the one in Unity) should work on a chunk of data to get certain factors
// TODO be able to set hist sizes, indicate update rate of Unity (and calculate DataListener::happenedNowThreshold from that)
public class MuseWrapper {

    static int EEGHistSize = 256; // TODO runtime adjustable
    static int ARHistSize = 256;
    static int AccHistSize = 256;

    private HashMap<Integer, Muse> devices = new HashMap<Integer, Muse>();
    private HashMap<Integer, ConnectionListener> connectionListeners = new HashMap<Integer, ConnectionListener>();
    private HashMap<Integer, DataListener> dataListeners = new HashMap<Integer, DataListener>();
    private boolean dataTransmission = true;
    private List<String> devIds = new ArrayList<String>();

    private static MuseWrapper instance;
    public static MuseWrapper getInstance() {
        if (instance == null)
            instance = new MuseWrapper();
        return instance;
    }

    public MuseWrapper() {}

    /// INTERFACE
    public boolean refresh() {
        MuseManager.refreshPairedMuses();
        List<Muse> pairedMuses = MuseManager.getPairedMuses();
        devIds.clear();
        for (Muse m: pairedMuses) {
            String devId = m.getName() + "-" + m.getMacAddress();
            devIds.add(devId);
        }
        return pairedMuses.size() > 0;
    }

    public int getNumOfDevices() {
        MuseManager.refreshPairedMuses();
        return MuseManager.getPairedMuses().size();
    }

    public String getConnectionState(int id) {
        return connectionListeners.get(id).getCurrentState().toString();
    }

    public boolean connect(int id) {
        List<Muse> pairedMuses = MuseManager.getPairedMuses();
        if (pairedMuses.size() <= id) {
            // TODO error
            return false;
        }

        connectionListeners.put(id, new ConnectionListener());
        dataListeners.put(id, new DataListener(EEGHistSize, ARHistSize, AccHistSize));
        devices.put(id, pairedMuses.get(id));
        //~muse = pairedMuses.get(id);
        ConnectionState state = devices.get(id).getConnectionState();
        if (state == ConnectionState.CONNECTED || state == ConnectionState.CONNECTING) { // TODO possible para itt, mindig azt hiszi hogy konnektált
            return true; // connect second time --> do nothing
        }

        configure_library(id);
        try {
            devices.get(id).runAsynchronously();
        } catch (Exception e) {
            // TODO log exception
            return false;
        }
        return true;
    }

    public void disconnect(int id) {
        devices.get(id).disconnect(true);
    }

    public void pause(int id) {
        dataTransmission = !dataTransmission;
        if (devices.containsKey(id))
            devices.get(id).enableDataTransmission(dataTransmission);
    }

    public EEGpacket pollEEG(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGpacket();
        return null;
    }

    public EEGpacket pollAlphaAbsolute(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGAlphaAbsolutePacket();
        return null;
    }
    public EEGpacket pollAlphaRelative(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGAlphaRelativePacket();
        return null;
    }
    public EEGpacket pollAlphaScore(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGAlphaScorePacket();
        return null;
    }

    public EEGpacket pollBetaAbsolute(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGBetaAbsolutePacket();
        return null;
    }
    public EEGpacket pollBetaRelative(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGBetaRelativePacket();
        return null;
    }
    public EEGpacket pollBetaScore(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGBetaScorePacket();
        return null;
    }

    public EEGpacket pollDeltaAbsolute(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGDeltaAbsolutePacket();
        return null;
    }
    public EEGpacket pollDeltaRelative(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGDeltaRelativePacket();
        return null;
    }
    public EEGpacket pollDeltaScore(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGDeltaScorePacket();
        return null;
    }

    public EEGpacket pollGammaAbsolute(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGGammaAbsolutePacket();
        return null;
    }
    public EEGpacket pollGammaRelative(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGGammaRelativePacket();
        return null;
    }
    public EEGpacket pollGammaScore(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGGammaScorePacket();
        return null;
    }

    public EEGpacket pollThetaAbsolute(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGThetaAbsolutePacket();
        return null;
    }
    public EEGpacket pollThetaRelative(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGThetaRelativePacket();
        return null;
    }
    public EEGpacket pollThetaScore(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollEEGThetaScorePacket();
        return null;
    }

    public AccPacket pollAccelerometer(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).pollAccelerometer();
        return null;
    }
    public boolean isHeadbandOn(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).isHeadbandOn();
        return false;
    }
    public boolean jawClenched(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).jawClenched();
        return false;
    }
    public boolean blinked(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).blinked();
        return false;
    }
    public double battery(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).getBattery();
        return 0;
    }
    public EEGpacket horseshoe(int id) {
        if (dataListeners.containsKey(id))
            return dataListeners.get(id).getHorseshoe();
        return null;
    }

    /// Private stuff
    private void configure_library(int id) {
        devices.get(id).registerConnectionListener(connectionListeners.get(id));
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.ACCELEROMETER);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.BATTERY);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.HORSESHOE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.EEG);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.ALPHA_RELATIVE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.ALPHA_ABSOLUTE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.ALPHA_SCORE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.BETA_ABSOLUTE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.BETA_RELATIVE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.BETA_SCORE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.GAMMA_ABSOLUTE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.GAMMA_RELATIVE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.GAMMA_SCORE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.DELTA_ABSOLUTE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.DELTA_RELATIVE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.DELTA_SCORE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.THETA_ABSOLUTE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.THETA_RELATIVE);
        devices.get(id).registerDataListener(dataListeners.get(id), MuseDataPacketType.THETA_SCORE);

        devices.get(id).setPreset(MusePreset.PRESET_14);
        devices.get(id).enableDataTransmission(dataTransmission);
    }
}